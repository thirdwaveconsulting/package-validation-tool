/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Utility Class for various Excel functions (parsing, formatting etc.)
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-10-31		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class PackageValidationExcel
{
	// **************************** //
	// ** REGEX & MISC CONSTANTS ** //
	// **************************** //

	private static final String PATTERN_NON_ALPHA = '/^[A-z]+$/';

	//////////////////////////////////////////
	// * READ AND FORMAT CSV INPUT STRING * //
	//////////////////////////////////////////

	public static List<List<String>> ParseCSVString(String sCSVData)
	{
		// Initialize All Cells & Line List
		List<List<String>> lAllCells = new List<List<String>>();
		List<String> lAllLines = new List<String>();

		// Initialize Field List & Single Line
		List<String> lAllFields = new List<String>();
		List<String> lSingleLine = new List<String>();

		// Handle Double Quotes with Commas in Fields
		sCSVData = sCSVData.replaceAll(',"""', ',"DBLQT').replaceAll('""",', 'DBLQT",');
		sCSVData = sCSVData.replaceAll('""', 'DBLQT');

		// Split Lines from Input String
		lAllLines = sCSVData.split('\n');

		// Initialize Single Line & Single Field String, Field Counter & Line Counter
		String sOneLine = '';
		String sOneField = '';

		// Initialize Line Counter & Field Counter
		Integer iLineCount = 0;
		Integer iCount = 0;

		// Run Through Lines
		for(iLineCount = 0; iLineCount < lAllLines.size(); iLineCount++)
		{
			// Get Single Line
			sOneLine = lAllLines[iLineCount];

			// Ignore Empty Lines
			if(sOneLine.replaceAll(',', '').trim().length() == 0) break;

			// Initialize Line
			lSingleLine = new List<String>();

			// Get All Fields for this Line
			lAllFields = sOneLine.split(',');

			// Run Through Fields (Cells)
			for(iCount = 0; iCount < lAllFields.size(); iCount++)
			{
				// Get Field
				sOneField = lAllFields[iCount];

				// API Name?
				if(iCount == 0)
				{
					sOneField = sOneField.replaceAll('[^A-Za-z]', '');
				}

				// Add Field to Single Line
				lSingleLine.add(sOneField);
			}

			// Add Line Number to Line
			lSingleLine.add(String.valueOf(iLineCount + 1));

			// Add Line to Final List
			lAllCells.add(lSingleLine);
		}

		// Return Formatted Cells
		return lAllCells;
	}

	/////////////////////////////////////////////
	// * SORT ALL EXCEL LINES ALPHABETICALLY * //
	/////////////////////////////////////////////

	public static List<List<String>> SortExcelLines(List<List<String>> lOriginalList)
	{
		// Initialize Sorted List
		List<List<String>> lSortedList = new List<List<String>>();

		// Initialize Map, List for Sorting & Index Key
		Map<String, List<String>> mapSortValues = new Map<String, List<String>>();
		List<String> lSortValues = new List<String>();
		String sOneKey = '';

		// Remove First Line (Header Line)
		lOriginalList.remove(0);

		// Run Through Lines
		for(List<String> lOneLine : lOriginalList)
		{
			// Get Key Value
			sOneKey = lOneLine[0].toLowerCase() + PackageValidationGenerator.GenerateXmlName(lOneLine[0], lOneLine[3].toLowerCase(), lOneLine[2].toLowerCase(), lOneLine[1].toLowerCase());

			// Assign to Map & List (if NOT found)
			if(mapSortValues.get(sOneKey) == null)
			{
				mapSortValues.put(sOneKey, lOneLine);
				lSortValues.add(sOneKey);
			}
		}

		// Sort Indexes --> Ascending
		lSortValues.sort();

		// Run Through Sorted Index
		for(String sOneIndex : lSortValues)
		{
			// Add to Sorted List
			lSortedList.add(mapSortValues.get(sOneIndex));
		}

		// Return Sorted List
		return lSortedList;
	}
}