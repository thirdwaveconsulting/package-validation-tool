/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Implementation of MetadataComponentValidator for fetching & validating deployment package components
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-11-16		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class ValidateApexComponent implements PackageValidationGenerator.MetadataComponentValidator
{
	/////////////////////////////////////////////////////////////////////
	// * VALIDATE METADATA COMPONENTS AGAINST COMPONENT MAP PROVIDED * //
	/////////////////////////////////////////////////////////////////////

	public static void ValidateComponents(List<PackageValidationWrapper.ValidationLineWrapper> lComponents, Map<String, Map<String, String>> mapValidComponents)
	{
		// Run Through Components
		for(PackageValidationWrapper.ValidationLineWrapper oOneWrapper : lComponents)
		{
			// Component Found?
			if(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()) != null)
			{
				// Set NOT FOUND Flag --> Valid!
				oOneWrapper.setNotFound(false);

				// Assign Prefix & Suffix
				oOneWrapper.setComponentPrefix(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_PREFIX));
				oOneWrapper.setComponentSuffix(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_SUFFIX));

				// Assign to Final Component with fixed Casing
				oOneWrapper.setFinalComponentName(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_NAMES));
			}

			// Set Processed Flag
			oOneWrapper.setIsProcessed(true);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	// * RETRIEVE & BUILD MAP OF VALID COMPONENTS FROM ENDPOINT PROVIDED ON WRAPPER * //
	////////////////////////////////////////////////////////////////////////////////////

	public static Map<String, Map<String, String>> RetrieveValidComponents(PackageValidationWrapper oWrapper, List<PackageValidationWrapper.ValidationLineWrapper> lComponents)
	{
		// Initialize Map for Storing valid Components
		Map<String, Map<String, String>> mapValidComponents = new Map<String, Map<String, String>>();

		// Metadata API Response & Individual XML Node
		HttpResponse oResponse;
		DOM.XmlNode oNode;

		// Initialize Component Name
		String sComponentName = '';

		// Send Query Request & Get Result Node
		oResponse = PackageValidationUtilities.SendQueryRequest(oWrapper.sAccessToken, oWrapper.sAccessEndpoint, 'SELECT Id, Name FROM ApexComponent LIMIT 1000', oWrapper.dApiVersion);
		oNode = oResponse.getBodyDocument().getRootElement().getChildElement('Body', PackageValidationUtilities.SOAP_NAMESPACE).getChildElement('queryResponse', PackageValidationUtilities.TARGET_NAMESPACE_ENTERPRISE).getChildElement('result', PackageValidationUtilities.TARGET_NAMESPACE_ENTERPRISE);

		// Run Through Query Results
		for(DOM.XmlNode oOneChild : oNode.getChildElements())
		{
			// Individual Record?
			if(oOneChild.getName() == 'records')
			{
				// Get Component Name
				sComponentName = oOneChild.getChildElement('Name', PackageValidationUtilities.TARGET_NAMESPACE_ENTERPRISE_OBJECT).getText();

				// Initialize Component Sub-Map
				mapValidComponents.put(sComponentName.toLowerCase(), new Map<String, String>());

				// Add Component Name, Prefix & Suffix to Map
				mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_NAMES, sComponentName);
				mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_PREFIX, '');
				mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_SUFFIX, '');
			}
		}

		// Return Valid Components Map
		return mapValidComponents;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// * GENERATE CORRECT XML NAME USED IN DEPLOYMENT PACKAGES FOR A GIVEN COMPONENT * //
	/////////////////////////////////////////////////////////////////////////////////////

	public static String GenerateXmlName(String sMetadataType, String sEntityApiName, String sEntityLabel, String sObjectApiName)
	{
		// Initialize XML Name
		String sXmlName = sEntityApiName;

		// Return Final Value
		return sXmlName;
	}
}