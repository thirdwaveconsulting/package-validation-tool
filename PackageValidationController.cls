/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Controller for validating deployment packages
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-10-11		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class PackageValidationController
{
	///////////////////////////////////////////////
	// * WRAPPER OBJECT & UPLOAD FILE SETTINGS * //
	///////////////////////////////////////////////

	public PackageValidationWrapper oWrapper {get; private set;}
	public Blob oFileUpload {get; set;}

	public String sFileName {get; set;}
	public Integer iFileSize {get; set;}

	////////////////////////////////////
	// * CONNECTION & AUTH SETTINGS * //
	////////////////////////////////////

	public String sAuthorizationEndpoint {get; private set;}
	public String sAuthorizationCode {get; private set;}

	public String sSavedSiteId {get; private set;}
	public String sLoginInstance {get; set;}
	public String sLoginApiVersion {get; set;}

	/////////////////////////////////////
	// * DEPLOYMENT PACKAGE SETTINGS * //
	/////////////////////////////////////

	public String sDeploymentZip {get; set;}
	public String sDeploymentPackageXml {get; set;}

	public Boolean bUpsertRequired {get; private set;}
	public Boolean bRefreshMetadata {get; set;}

	/////////////////////////////////
	// * STATUS & ERROR MESSAGES * //
	/////////////////////////////////

	public String sStatusMessage {get; private set;}
	public String sErrorMessage {get; private set;}

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public PackageValidationController()
	{
		// Initialize All Object Data
		InitializeObjectData();

		// Verify Authentication Settings
		VerifyAuthentication();

		// Verify Remote Site Settings
		VerifyRemoteSiteSettings();
	}

	////////////////////////////////////
	// * INITIALIZE ALL OBJECT DATA * //
	////////////////////////////////////

	private void InitializeObjectData()
	{
		// Initialize Wrapper Object
		this.oWrapper = new PackageValidationWrapper();

		// Set Default Login Instance (PRODUCTION) & API Version (Latest)
		this.sLoginInstance = 'login';
		this.sLoginApiVersion = String.valueOf(PackageValidationUtilities.DEFAULT_API_VERSION);

		// Initialize Status & Error Message
		this.sStatusMessage = '';
		this.sErrorMessage = '';

		// Initialize Upsert Flag & Refresh Flag
		this.bUpsertRequired = false;
		this.bRefreshMetadata = true;
	}

	////////////////////////////////////////
	// * VERIFY AUTHENTICATION SETTINGS * //
	////////////////////////////////////////

	private void VerifyAuthentication()
	{
		// Set Authorization Code
		this.sAuthorizationCode = ApexPages.currentPage().getParameters().get('code');

		// OAuth Data NOT Set AND Authorization Code is Set?
		if(this.oWrapper.sAccessEndpoint == '' && this.sAuthorizationCode != null)
		{
			// Set API Version on Page AND Wrapper using State parameter
			this.sLoginApiVersion = ApexPages.currentPage().getParameters().get('state').substringAfter('-');
			this.oWrapper.setApiVersion(Decimal.valueOf(ApexPages.currentPage().getParameters().get('state').substringAfter('-')));

			// Get Access Token Data from OAUTH
			this.sErrorMessage = GetAccessToken();
		}

		// Error Message is Set?
		if(this.sErrorMessage != '')
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, sErrorMessage));
		}
	}

	////////////////////////////////////////
	// * VERIFY AUTHENTICATION SETTINGS * //
	////////////////////////////////////////

	private void VerifyRemoteSiteSettings()
	{
		// List of Saved Sites (Cached Remote Site Settings)
		List<Saved_Site__c> lSavedSites;

		// OAuth Data Set AND No previous Error?
		if(this.oWrapper.sAccessEndpoint != '' && this.sErrorMessage == '')
		{
			// Check if Remote Site exists in Cache (Saved Sites Object)
			lSavedSites = [SELECT Id FROM Saved_Site__c WHERE gman_twc__Remote_Site__c = :this.oWrapper.sAccessEndpoint LIMIT 1];

			// Site NOT Found?
			if(lSavedSites.size() == 0)
			{
				this.sStatusMessage = 'Verifying Remote Site Settings...';

				// Set Request Id (for polling)
				this.oWrapper.setRequestId(PackageValidationUtilities.GetRetrieveRemoteSiteSettingsProcessId(UserInfo.getSessionId(), this.oWrapper.dApiVersion));

				// Clear Authorization Code (IF Request is invalid from Exception thrown)
				this.sAuthorizationCode = (this.oWrapper.sRequestId == '') ? '' : this.sAuthorizationCode;

				// Request Upsert when Validating
				this.bUpsertRequired = true;
			}
			else
			{
				// Set Site Id
				this.sSavedSiteId = lSavedSites[0].Id;
			}
		}
	}

	/////////////////////////////////////////////
	// * UPSERT CACHE RECORD FOR REMOTE SITE * //
	/////////////////////////////////////////////

	private void UpsertSavedSiteRecord(String sEndpoint)
	{
		// Initialize Site Record & Set Data
		Saved_Site__c oSavedSite = new Saved_Site__c();
		oSavedSite.Remote_Site__c = sEndpoint.toLowerCase();

		try
		{
			// Upsert Record
			upsert oSavedSite;

			// Set Site Id
			this.sSavedSiteId = oSavedSite.Id;

			// Clear Upsert Request Flag
			this.bUpsertRequired = false;
		}
		catch(Exception ex)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error UPSERTING Saved Site: ' + ex.getMessage()));
		}
	}

	///////////////////////////////////////////////
	// * SEND REQUEST FOR ACCESS TOKEN (OAUTH) * //
	///////////////////////////////////////////////

	private String GetAccessToken()
	{
		// Response Data
		HttpResponse oResponse = PackageValidationUtilities.SendAuthenticationRequest(this.sAuthorizationCode, 'https://' + ApexPages.currentPage().getParameters().get('state').substringBefore('-') + '.salesforce.com/services/oauth2/token', URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CreateDeploymentPackage');
		Map<String, String> mapResponse;

		// Initialize Response
		String sResponse = '';

		// Success?
		if(oResponse != null && oResponse.getStatusCode() == PackageValidationUtilities.HTTP_RESPONSE_CODE_SUCCESS)
		{
			// Get Response Data
			mapResponse = (Map<String, String>)JSON.deserialize(oResponse.getBody(), Map<String, String>.class);

			// Set OAUTH Data on Wrapper
			this.oWrapper.setAccessToken(mapResponse.get('access_token'));
			this.oWrapper.setRefreshToken(mapResponse.get('refresh_token'));
			this.oWrapper.setAccessEndpoint(mapResponse.get('instance_url'));

			// Set Endpoint
			this.sAuthorizationEndpoint = mapResponse.get('instance_url');
		}
		else
		{
			sResponse = 'Error Sending Authentication Request' + ((oResponse != null) ? ': ' + oResponse.getBody() : '');
		}

		// Return Final Response
		return sResponse;
	}

	//////////////////////////////////////////
	// * REDIRECT TO LOGIN PAGE FOR OAUTH * //
	//////////////////////////////////////////

	public PageReference LoginRedirect()
	{
		// Create Page Reference with Endpoint specified by Environment selected
		PageReference oRedirect = new PageReference('https://' + this.sLoginInstance + '.salesforce.com' + '/services/oauth2/authorize?response_type=code&client_id=' + '3MVG9uudbyLbNPZMdFyxlvjObzZzQ8.qAAv4EEMJlKh63vbq7DaD52ZgpnaxHUBwhA_vFC6DxH2doDGlIfV9K' + '&redirect_uri=' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CreateDeploymentPackage' + '&prompt=' + 'login' + '&state=' + this.sLoginInstance + '-' + this.sLoginApiVersion);

		// Redirect to Login Page
		return oRedirect;
	}

	////////////////////////////////////////////
	// * POLL FOR METADATA RETRIEVE RESULTS * //
	////////////////////////////////////////////

	public PageReference PollMetadataRetrieve()
	{
		// Verify Retrieve Status & Update Status Message (IF required)
		this.sStatusMessage = this.oWrapper.VerifyRetrieveStatus(this.sStatusMessage, this.sDeploymentZip);

		// No Redirect
		return null;
	}

	///////////////////////////////////////////
	// * VALIDATE EXCEL FILE GIVEN BY USER * //
	///////////////////////////////////////////

	public PageReference ValidateExcelFile()
	{
		// Site was NOT Found --> Upsert Requested
		if(this.bUpsertRequired == true && sAuthorizationEndpoint != '')
		{
			UpsertSavedSiteRecord(sAuthorizationEndpoint);
		}

		// Initialize Wrapper Object's Excel Data
		this.oWrapper.InitializeExcelData();

		// No File Selected?
		if(this.oFileUpload == null)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'No file selected, please try again.'));
			return null;
		}

		// Set Refresh Metadata Flag
		this.oWrapper.setRefreshValidComponents(this.bRefreshMetadata);

		// Build Excel String & Format
		return BuildExcelString();
	}

	//////////////////////////////////////////////////////
	// * BUILD UPLOAD DATA USING FILE UPLOAD PROVIDED * //
	//////////////////////////////////////////////////////

	private PageReference BuildExcelString()
	{
		// Redirect Flag
		Boolean bIsValid = false;

		// Initialize CSV String
		String sUploadData = '';

		try
		{
			// Convert BLOB to String (only works for UTF-8 Encoded)
			sUploadData = this.oFileUpload.toString();

			// Set Redirect Flag
			bIsValid = true; 
		}
		catch(Exception ex)
		{
			// Invalid Encoding on file?
			if(ex.getMessage() == 'BLOB is not a valid UTF-8 string')
			{
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid formatting. Please check that the CSV file is UTF-8'));
			}
			else
			{
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Unable to read Excel file: ' + ex.getMessage()));
			}
		}

		// Return Redirect
		return (bIsValid == false) ? null : FormatExcelData(sUploadData);
	}

	//////////////////////////////////////////////////////////
	// * FORMAT ALL EXCEL DATA ASSIGNED TO WRAPPER OBJECT * //
	//////////////////////////////////////////////////////////

	private PageReference FormatExcelData(String sUploadData)
	{
		// Initialize Return Result (Redirect)
		PageReference oResult;

		// Parse and Format CSV Data & Store in Wrapper
		this.oWrapper.setExcelData(PackageValidationExcel.ParseCSVString(sUploadData));
		this.oWrapper.setExcelString(sUploadData);

		// Verify Data Map Contents
		VerifyMapContents();

		// Valid Metadata Types Found?
		if(this.oWrapper.mapMetadataTypes.size() > 0)
		{
			// Validate Metadata Types in Excel Data & Set Package XML
			this.oWrapper.ValidateExcelData();
			this.sDeploymentPackageXml = this.oWrapper.getPackageXml();

			// No Redirect Required
			oResult = null;
		}
		else
		{
			// Reload Page without Auth Code
			oResult = Page.CreateDeploymentPackage;
		}

		return oResult;
	}

	////////////////////////////////////////////////////////////
	// * VERIFY THE CONTENTS OF METADATA & OBJECT LIST MAPS * //
	////////////////////////////////////////////////////////////

	private void VerifyMapContents()
	{
		// Metadata Types Missing?
		if(this.oWrapper.mapMetadataTypes == null)
		{
			// Get List of Metadata Types
			this.oWrapper.RetrieveMetadataTypes();
		}

		// Full Object List Missing OR Refresh requested?
		if(this.oWrapper.mapObjectList == null || this.oWrapper.bRefreshValidComponents == true)
		{
			// Get List of SObjects
			this.oWrapper.RetrieveFullObjectList();
		}

		// Full Tab List Missing OR Refresh requested?
		if(this.oWrapper.mapTabList == null || this.oWrapper.bRefreshValidComponents == true)
		{
			// Get List of Tabs
			this.oWrapper.RetrieveFullTabList();
		}

		// Clear Refresh Flag
		this.bRefreshMetadata = false;
	}

	////////////////////////////////////////////////////////
	// * RESET & CLEAR UPLOAD DATA & PACKAGE VALIDATION * //
	////////////////////////////////////////////////////////

	public PageReference ClearUploadData()
	{
		// Clear Excel Data & Package String on Wrapper Object
		this.oWrapper.InitializeExcelData();
		this.oWrapper.setExcelData(new List<List<String>>());
		this.oWrapper.setPackageXml('');

		// Clear File Upload
		this.oFileUpload = null; 

		// No Redirect
		return null;
	}

	///////////////////////////
	// * GETTERS & SETTERS * //
	///////////////////////////

	public List<SelectOption> getApiVersions()
	{
		return PackageValidationUtilities.GetApiVersions();
	}

	public String getPackageXmlForSavedSite()
	{
		return this.oWrapper.BuildMetadataPackageXml();
	}

	public String getElementXmlForSavedSite()
	{
		return this.oWrapper.BuildMetadataSavedSiteXml();
	}

	public String getRemoteSiteApiName()
	{
		return this.oWrapper.getRemoteSiteApiName();
	}

	public String getCurrentRequestId()
	{
		return this.oWrapper.sRequestId;
	}

	public String getFormattedLastRefreshDate()
	{
		return (this.oWrapper.dLastUpdated == null) ? 'NA' : this.oWrapper.dLastUpdated.format('MMMM d h:mm a');
	}

	public Integer getExcelDataSize()
	{
		return this.oWrapper.getExcelDataSize();
	}
}