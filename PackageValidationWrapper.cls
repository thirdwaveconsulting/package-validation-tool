/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Wrapper object to store package validation data
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-10-31		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class PackageValidationWrapper
{
	///////////////////////////////////////
	// * OAUTH TOKENS, ENDPOINTS & API * //
	///////////////////////////////////////

	public String sAccessToken {get; private set;} 
	public String sRefreshToken {get; private set;}

	public String sAccessEndpoint {get; private set;}
	public Decimal dApiVersion {get; private set;}

	////////////////////////////////////////
	// * METADATA SETTINGS & REFERENCES * //
	////////////////////////////////////////

	public Map<String, Map<String, Map<String, String>>> mapFullValidationData {get; private set;}
	public Map<String, String> mapObjectList {get; private set;}
	public Map<String, String> mapTabList {get; private set;}

	public Map<String, String> mapMetadataTypes {get; private set;}
	private List<String> lMetadataTypes;

	////////////////////////////////////////////////////////////////////////
	// * LAST UPDATED TIMESTAMP & FLAG FOR UPDATING VALID COMPONENT MAP * //
	////////////////////////////////////////////////////////////////////////

	public DateTime dLastUpdated {get; private set;}
	public Boolean bRefreshValidComponents {get; private set;}

	/////////////////////////////////////////////////
	// * GENERATED PACKAGE DOCUMENT & XML STRING * //
	/////////////////////////////////////////////////

	public transient DOM.Document oDocument {get; private set;}
	public String sFinalPackageString {get; private set;}

	///////////////////////////////////////////////////
	// * METADATA DEPLOYMENT & VALIDATION SETTINGS * //
	///////////////////////////////////////////////////

	public String sRequestId {get; private set;}
	public Integer iComponentErrorCount {get; private set;}

	private Boolean bIsRetrieved;
	private Boolean bDeployRequired;
	private Boolean bDeployRequested;

	///////////////////////////////////
	// * EXCEL MAPPINGS & SETTINGS * //
	///////////////////////////////////

	private List<List<String>> lAllExcelData;
	private String sFileData;

	public Map<String, List<ValidationLineWrapper>> mapAllComponents {get; private set;}
	public List<ValidationLineWrapper> lFullComponentList {get; private set;}
	public List<ValidationLineWrapper> lAllMetadataErrors {get; private set;}
	public List<String> lColumnHeaders {get; private set;}

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public PackageValidationWrapper()
	{
		// Initialize Object Data
		InitializeObjectData();

		// Initialize Excel Data
		InitializeExcelData();
	}

	////////////////////////////////
	// * INITIALIZE OBJECT DATA * //
	////////////////////////////////

	private void InitializeObjectData()
	{
		// Initialize Full Data Map & Metadata Types List
		this.mapFullValidationData = new Map<String, Map<String, Map<String, String>>>();
		this.lMetadataTypes = new List<String>();

		// Initialize Request Id
		this.sRequestId = '';

		// Initialize Deployment Flags & Refresh Valid Components Flag
		this.bIsRetrieved = false;
		this.bDeployRequired = false;
		this.bDeployRequested = false;
		this.bRefreshValidComponents = true;

		// Initialize API Version & Component Error Count
		this.dApiVersion = PackageValidationUtilities.DEFAULT_API_VERSION;
		this.iComponentErrorCount = 0;

		// Initialize OAuth Settings
		this.sAccessToken = '';
		this.sRefreshToken = '';
		this.sAccessEndpoint = '';
	}

	///////////////////////////////
	// * INITIALIZE EXCEL DATA * //
	///////////////////////////////

	public void InitializeExcelData()
	{
		// Initialize Excel Data
		this.mapAllComponents = new Map<String, List<ValidationLineWrapper>>();
		this.lFullComponentList = new List<ValidationLineWrapper>();
		this.lAllMetadataErrors = new List<ValidationLineWrapper>();
		this.lAllExcelData = new List<List<String>>();
		this.lColumnHeaders = new List<String>();

		// Initialize Original Excel Data
		this.sFileData = '';
	}

	///////////////////////////////////////////////////////////////////////////////
	// * RETRIEVE LIST OF METADATA TYPES VIA SOAP & ASSIGN TO OBJECT VARIABLES * //
	///////////////////////////////////////////////////////////////////////////////

	public void RetrieveMetadataTypes()
	{
		// Initialize Case Insensitive Metadata Map
		this.mapMetadataTypes = PackageValidationUtilities.BuildMetadataTypesList(this.sAccessToken, this.sAccessEndpoint, this.dApiVersion);

		// Run Through Metadata Types
		for(String sOneType : this.mapMetadataTypes.values())
		{
			// Initialize Full Data Map for this Type
			this.mapFullValidationData.put(sOneType, new Map<String, Map<String, String>>());

			// Add to List
			this.lMetadataTypes.add(sOneType);
		}

		// Sort List Ascending
		this.lMetadataTypes.sort();
	}

	//////////////////////////////////////////////////////////////////////////////
	// * RETRIEVE FULL LIST OF SOBJECTS VIA SOAP & ASSIGN TO OBJECT VARIABLES * //
	//////////////////////////////////////////////////////////////////////////////

	public void RetrieveFullObjectList()
	{
		// Set Case Insensitive Object Map
		this.mapObjectList = PackageValidationUtilities.BuildFullObjectList(this.sAccessToken, this.sAccessEndpoint, this.dApiVersion);
	}

	//////////////////////////////////////////////////////////////////////////
	// * RETRIEVE FULL LIST OF TABS VIA SOAP & ASSIGN TO OBJECT VARIABLES * //
	//////////////////////////////////////////////////////////////////////////

	public void RetrieveFullTabList()
	{
		// Set Case Insensitive Object Map
		this.mapTabList = PackageValidationUtilities.BuildFullTabList(this.sAccessToken, this.sAccessEndpoint, this.dApiVersion);
	}

	/////////////////////////////////////////////////////////////////////
	// * FETCH ALL VALID COMPONENTS WITH METADATA TYPE LIST PROVIDED * //
	/////////////////////////////////////////////////////////////////////

	public void FetchValidComponents()
	{
		// Fetch all valid components
		this.mapFullValidationData = PackageValidationGenerator.GenerateFullValidComponentsMap(this.mapAllComponents, this, new List<String>(this.mapAllComponents.keySet()));
	}

	//////////////////////////////////////////////////////////////////
	// * VALIDATE EXCEL DATA AGAINST THE GENERATED METADATA TYPES * //
	//////////////////////////////////////////////////////////////////

	public void ValidateExcelData()
	{
		// Remove Header Row, Sort Excel Data Lines & Initialize Document String
		this.lAllExcelData.remove(0);
		this.lAllExcelData = PackageValidationExcel.SortExcelLines(this.lAllExcelData);
		this.sFinalPackageString = '';

		// Initialize Single Line & Set Wrapper
		List<String> lOneLine = new List<String>();
		ValidationLineWrapper oOneWrapper;

		// Initialize Metadata String & Counter
		String sOneMetadata = '';
		Integer iCount = 0;

		// Run Through Lines
		for(iCount = 0; iCount < this.lAllExcelData.size(); iCount++)
		{
			// Get Line Record
			lOneLine = this.lAllExcelData[iCount];

			// Get Metadata Type
			sOneMetadata = lOneLine[0].toLowerCase().trim();

			// Invalid Metadata Type?
			if(this.mapMetadataTypes.get(sOneMetadata) == null)
			{
				// Initialize New Wrapper
				oOneWrapper = new ValidationLineWrapper(lOneLine[0], lOneLine[1], lOneLine[2], lOneLine[3], lOneLine[4], Integer.valueOf(lOneLine[5]));

				// Add to Error List
				this.lAllMetadataErrors.add(oOneWrapper);
			}
			// Valid Component --> Add to Component List
			else if(lOneLine[3] != null && lOneLine[3] != '')
			{
				// Fix Metadata Case
				sOneMetadata = this.mapMetadataTypes.get(sOneMetadata);

				// Initialize New Wrapper
				oOneWrapper = new ValidationLineWrapper(sOneMetadata, PackageValidationGenerator.CleanApiName(lOneLine[1]), lOneLine[2].trim(), PackageValidationGenerator.CleanApiName(lOneLine[3]), lOneLine[4].trim(), Integer.valueOf(lOneLine[5]));

				// Check if Map component is initialized
				if(this.mapAllComponents.get(sOneMetadata) == null)
				{
					// Initialize List Element
					this.mapAllComponents.put(sOneMetadata, new List<ValidationLineWrapper>());
				}

				// Add to Map
				this.mapAllComponents.get(sOneMetadata).add(oOneWrapper);

				// Add to Component List
				this.lFullComponentList.add(oOneWrapper);
			}
		}

		// No Metadata errors found?
		if(this.lAllMetadataErrors.size() == 0)
		{
			// Run All Validation
			RunFullValidation();

			// Check for Component Matches (for invalid Components)
			CheckInvalidComponentMatches();
		}
	}

	///////////////////////////////////////////////////////
	// * RUN ALL VALIDATION ROUTINES & PROVIDE SUMMARY * //
	///////////////////////////////////////////////////////

	private void RunFullValidation()
	{
		// Check if Metadata Requires Updating
		CheckMetadataRefreshStatus();

		// Check for Component Errors
		PackageValidationGenerator.ValidatePackageComponents(this, this.mapAllComponents, this.mapFullValidationData);

		// Get Component Valid Count
		CheckValidComponentCount();

		// No Metadata Errors?
		if(this.lAllMetadataErrors.size() == 0)
		{
			// Build XML Package
			this.oDocument = PackageValidationGenerator.BuildFinalXMLPackage(this.mapAllComponents, this.dApiVersion);
			this.sFinalPackageString = (this.oDocument == null) ? '' : this.oDocument.toXmlString();
		}
	}

	//////////////////////////////////////////////////////
	// * CHECK CLOSEST MATCHES FOR INVALID COMPONENTS * //
	//////////////////////////////////////////////////////

	private void CheckInvalidComponentMatches()
	{
		// Individual Component List 
		List<ValidationLineWrapper> lComponents;

		// Run Through All Components
		for(String sMetadataType : mapAllComponents.keySet())
		{
			// Get Component list for Metadata type
			lComponents = mapAllComponents.get(sMetadataType);

			// Run Through Component list
			for(ValidationLineWrapper oOneWrapper : lComponents)
			{
				// Component NOT Found?
				if(oOneWrapper.bNotFound == true)
				{
					// Get the Closest Match
					oOneWrapper.setClosestMatch(PackageValidationUtilities.FindClosestComponentMatch(oOneWrapper.sEntityApiName, this.mapFullValidationData.get(sMetadataType)));
				}
			}
		}
	}

	////////////////////////////////////////////////////////////
	// * CHECK METADATA REFRESH FLAG --> UPDATE IF REQUIRED * //
	////////////////////////////////////////////////////////////

	private void CheckMetadataRefreshStatus()
	{
		// Full Validation Map requested?
		if(this.bRefreshValidComponents == true)
		{
			// Fetch all valid components
			FetchValidComponents();

			// Set Fetch Date
			this.dLastUpdated = DateTime.now();

			// Clear Flag
			this.setRefreshValidComponents(false);
		}
	}

	////////////////////////////////////////////////////////////////////
	// * VERIFY ALL COMPONENT STATUS & RUN TOTALS (COMPONENT COUNT) * //
	////////////////////////////////////////////////////////////////////

	private void CheckValidComponentCount()
	{
		// Initialize Master List Count
		Map<String, Map<String, Integer>> mapInvalidCount = new Map<String, Map<String, Integer>>();

		// Initialize Totals & Metadata categories
		mapInvalidCount.put('metadata', new Map<String, Integer>());
		mapInvalidCount.get('metadata').put('###_TOTALS_###', 0);

		// Single Wrapper & Initialize Error Count
		ValidationLineWrapper oLineWrapper;
		this.iComponentErrorCount = 0;

		// Run Through Component List
		for(ValidationLineWrapper oOneWrapper : lFullComponentList)
		{
			// Check if Metadata type is initialized in Map
			if(mapInvalidCount.get('metadata').get(oOneWrapper.sMetadataType.toLowerCase()) == null)
			{
				// Initialize Entry
				mapInvalidCount.get('metadata').put(oOneWrapper.sMetadataType.toLowerCase(), 0);
			}

			// Increment Individual Metadata type AND Total Error Count
			mapInvalidCount.get('metadata').put(oOneWrapper.sMetadataType.toLowerCase(), mapInvalidCount.get('metadata').get(oOneWrapper.sMetadataType.toLowerCase()) + ((oOneWrapper.bNotFound == true) ? 1 : 0));
			mapInvalidCount.get('metadata').put('###_TOTALS_###', mapInvalidCount.get('metadata').get('###_TOTALS_###') + ((oOneWrapper.bNotFound == true) ? 1 : 0));
		}

		// Set Total Component Error Count
		this.iComponentErrorCount = mapInvalidCount.get('metadata').get('###_TOTALS_###');

		// Run Through Component Map
		for(String sOneMetadataType : mapAllComponents.keySet())
		{
			// Get First Element for given Metadata type
			oLineWrapper = mapAllComponents.get(sOneMetadataType).get(0);

			// Set Component Counts
			oLineWrapper.setInvalidComponents(mapInvalidCount.get('metadata').get(sOneMetadataType.toLowerCase()));
			oLineWrapper.setTotalComponents(mapAllComponents.get(sOneMetadataType).size());
		}
	}

	///////////////////////////////////////////////////////////////////
	// * VERIFY THAT REMOTE SITE SETTING EXISTS FOR GIVEN ENDPOINT * //
	///////////////////////////////////////////////////////////////////

	public String VerifyRetrieveStatus(String sCurrentResponse, String sEncodedZipData)
	{
		// Response to return (DEFAULT --> Current Response)
		String sReturnResponse = sCurrentResponse;

		// Retrieve pending?
		if(this.bIsRetrieved == false)
		{
			// Check Response
			GetMetadataRetrieveResponse();
		}
		// Retrieve Finished --> Remote Site needs to be added?
		else if(this.bDeployRequired == true)
		{
			// Update Status Message
			sReturnResponse = 'Inserting Remote Site entry...';

			// Check Response
			GetMetadataDeployResponse(sEncodedZipData);
		}
		else
		{
			// Clear Request Id
			this.sRequestId = '';
		}

		// Return Response Message
		return sReturnResponse;
	}

	///////////////////////////////////////////////////////////////
	// * GET METADATA RETRIEVE RESPONSE & SET DEPLOYMENT FLAGS * //
	///////////////////////////////////////////////////////////////

	private void GetMetadataRetrieveResponse()
	{
		// Store Fullnames of Remote Site Settings
		Set<String> setSiteNames = new Set<String>();

		// Check Retrieve Status for Remote Site Settings
		HttpResponse oResponse = PackageValidationUtilities.GetCheckRetrieveStatus(UserInfo.getSessionId(), this.sRequestId, false, this.dApiVersion);

		// Get Result Node
 		DOM.XmlNode oResult = oResponse.getBodyDocument().getRootElement().getChildElement('Body', PackageValidationUtilities.SOAP_NAMESPACE).getChildElement('checkRetrieveStatusResponse', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getChildElement('result', PackageValidationUtilities.TARGET_NAMESPACE_METADATA);

		// Request has finished?
		if(oResult == null || oResult.getChildElement('done', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getText() == 'true')
		{
			// Set Retrieved Flag
			this.bIsRetrieved = true;

			// Run Through Response XML Elements
			for(DOM.XmlNode oOneChild : oResult.getChildElements())
			{
				// Individual Remote Site Setting Properties?
				if(oOneChild.getName() == 'fileProperties' && oOneChild.getChildElement('type', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getText() == 'RemoteSiteSetting')
				{
					// Add Site to Set
					setSiteNames.add(oOneChild.getChildElement('namespacePrefix', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getText() + '__' + oOneChild.getChildElement('fullName', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getText());
				}
			}

			// Remote Site NOT Found in Metadata?
			if(!PackageValidationUtilities.RemoteSiteExists(setSiteNames, this.sAccessEndpoint, this.dApiVersion))
			{
				// Set Deployment Flag
				this.bDeployRequired = true;
			}
		}
	}

	//////////////////////////////////////
	// * GET METADATA DEPLOY RESPONSE * //
	//////////////////////////////////////

	private void GetMetadataDeployResponse(String sEncodedZipData)
	{
		// Metadata API Response
		HttpResponse oResponse;

		// Result Node
		DOM.XmlNode oResult;

		// Deployment NOT yet Requested?
		if(this.bDeployRequested == false)
		{
			// Send Request for deployment of RemoteSiteSetting
			oResponse = PackageValidationUtilities.SendDeployRemoteSiteRequest(UserInfo.getSessionId(), sEncodedZipData, this.dApiVersion);

			// Get New Request Id
 			this.sRequestId = oResponse.getBodyDocument().getRootElement().getChildElement('Body', PackageValidationUtilities.SOAP_NAMESPACE).getChildElement('deployResponse', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getChildElement('result', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getChildElement('id', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getText();

			// Clear Flag
			this.bDeployRequested = true;
		}

		// Check Deploy Status for Remote Site Settings
		oResponse = PackageValidationUtilities.GetCheckDeployStatus(UserInfo.getSessionId(), this.sRequestId, this.dApiVersion);

		// Get Done Node in Result
		oResult = oResponse.getBodyDocument().getRootElement().getChildElement('Body', PackageValidationUtilities.SOAP_NAMESPACE).getChildElement('checkDeployStatusResponse', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getChildElement('result', PackageValidationUtilities.TARGET_NAMESPACE_METADATA).getChildElement('done', PackageValidationUtilities.TARGET_NAMESPACE_METADATA);

		// Deploy Finished?
		if(oResult.getText() == 'true')
		{
			// Clear Request Id
			this.sRequestId = '';
		}
	}

	//////////////////////////////////////////////////////
	// * BUILD METADATA DEPLOYMENT PACKAGE XML STRING * //
	//////////////////////////////////////////////////////

	public String BuildMetadataPackageXml()
	{
		// Initialize Metadata List
		Map<String, List<String>> mapMetadata = new Map<String, List<String>>();

		// Add Remote Site to Metadata Map
		mapMetadata.put('RemoteSiteSetting', new List<String>{this.getRemoteSiteApiName()});

		return PackageValidationGenerator.GenerateDeploymentPackage(mapMetadata, this.dApiVersion).toXmlString();
	}

	/////////////////////////////////////////////////////////////
	// * BUILD METADATA XML STRING FOR SAVED SITE ON WRAPPER * //
	/////////////////////////////////////////////////////////////

	public String BuildMetadataSavedSiteXml()
	{
		return PackageValidationGenerator.GenerateSingleRemoteSiteSetting(this.sAccessEndpoint, false, true).toXmlString();
	}

	////////////////////////////////////////
	// * WRAPPER CLASS FOR PACKAGE LINE * //
	////////////////////////////////////////

	public class ValidationLineWrapper
	{
		//////////////////////////
		// * MAIN OBJECT DATA * //
		//////////////////////////

		public String sMetadataType {get; private set;}
		public String sObjectApiName {get; private set;}
		public String sEntityLabel {get; private set;}
		public String sEntityApiName {get; private set;}
		public String sAuthorName {get; private set;}
		public Integer iLineNumber {get; private set;}

		//////////////////////////////////////
		// * MATCH & ERROR VALUES (FLAGS) * //
		//////////////////////////////////////

		public String sClosestMatch {get; private set;}
		public Boolean bIsProcessed {get; private set;}
		public Boolean bNotFound {get; private set;}

		//////////////////////
		// * SUMMARY DATA * //
		//////////////////////

		public Integer iInvalidComponents {get; private set;}
		public Integer iTotalComponents {get; private set;}

		////////////////////////////////////////////////////
		// * CLEAN COMPONENT NAME WITH PREFIX & SUFFIX  * //
		////////////////////////////////////////////////////

		public String sFinalComponentName {get; private set;}
		public String sComponentPrefix {get; private set;}
		public String sComponentSuffix {get; private set;}

		//////////////////////////
		// * MAIN CONSTRUCTOR * //
		//////////////////////////

		public ValidationLineWrapper(String sMetadata, String sObjectApi, String sDisplay, String sLineApi, String sAuthor, Integer iExcelLine)
		{
			// Set Main Object Values
			this.sMetadataType = sMetadata;
			this.sObjectApiName = sObjectApi;
			this.sEntityLabel = sDisplay;
			this.sEntityApiName = sLineApi;
			this.sAuthorName = sAuthor;
			this.iLineNumber = iExcelLine;

			// Match & Error Values
			this.sClosestMatch = '';
			this.bIsProcessed = false;
			this.bNotFound = true;

			// Initialize Summary Data
			this.iInvalidComponents = 0;
			this.iTotalComponents = 0;

			// Initialize Final Component Values
			this.sFinalComponentName = PackageValidationGenerator.GenerateXmlName(this.sMetadataType, this.sEntityApiName, this.sEntityLabel, this.sObjectApiName);
			this.sComponentPrefix = '';
			this.sComponentSuffix = '';
		}

		///////////////////////////
		// * GETTERS & SETTERS * //
		///////////////////////////

		public void setEntityApiName(String sNewApiName)
		{
			this.sEntityApiName = sNewApiName;
		}

		public void setClosestMatch(String sNewMatchName)
		{
			this.sClosestMatch = sNewMatchName;
		}

		public void setIsProcessed(Boolean bNewValue)
		{
			this.bIsProcessed = bNewValue;
		}

		public void setNotFound(Boolean bNewValue)
		{
			this.bNotFound = bNewValue;
		}

		public void setFinalComponentName(String sComponentName)
		{
			this.sFinalComponentName = sComponentName;
		}

		public void setComponentPrefix(String sNewPrefix)
		{
			this.sComponentPrefix = sNewPrefix;
		}

		public void setComponentSuffix(String sNewSuffix)
		{
			this.sComponentSuffix = sNewSuffix;
		}

		public void setInvalidComponents(Integer iInvalidComponentCount)
		{
			this.iInvalidComponents = iInvalidComponentCount;
		}

		public void setTotalComponents(Integer iTotalComponentCount)
		{
			this.iTotalComponents = iTotalComponentCount;
		}

		public String getValidImage()
		{
			return (this.bNotFound == true) ? '/img/func_icons/remove12_on.gif' : '/img/func_icons/util/checkmark16.gif';
		}

		public String getAllValidImage()
		{
			return (this.iInvalidComponents > 0) ? '/img/func_icons/remove12_on.gif' : '/img/func_icons/util/checkmark16.gif';
		}

		public Integer getValidComponentSize()
		{
			return this.iTotalComponents - this.iInvalidComponents;
		}
	}

	///////////////////////////
	// * GETTERS & SETTERS * //
	///////////////////////////

	public void setExcelData(List<List<String>> lFormattedData)
	{
		this.lAllExcelData = lFormattedData;
	}

	public void setExcelString(String sOriginalData)
	{
		this.sFileData = sOriginalData;
	}

	public void setAccessToken(String sNewAccessToken)
	{
		this.sAccessToken = sNewAccessToken;
	}

	public void setRefreshToken(String sNewRefreshToken)
	{
		this.sRefreshToken = sNewRefreshToken;
	}

	public void setAccessEndpoint(String sNewAccessEndpoint)
	{
		this.sAccessEndpoint = sNewAccessEndpoint;
	}

	public void setRequestId(String sProcessId)
	{
		this.sRequestId = sProcessId;
	}

	public void setPackageXml(String sNewPackageData)
	{
		this.sFinalPackageString = sNewPackageData;
	}

	public void setApiVersion(Decimal dNewApiVersion)
	{
		this.dApiVersion = dNewApiVersion;
	}

	public void setRefreshValidComponents(Boolean bNewValue)
	{
		this.bRefreshValidComponents = bNewValue;
	}

	public List<List<String>> getExcelData()
	{
		return this.lAllExcelData;
	}

	public String getPackageXml()
	{
		return this.sFinalPackageString;
	}

	public String getRemoteSiteApiName()
	{
		// Initialize Remote API Name
		String sRemoteApiName = 'PV_' + PackageValidationUtilities.GetCleanInstanceBaseUrl(this.sAccessEndpoint).substringAfter('//');

		// Remove Dashes & Dots
		sRemoteApiName = sRemoteApiName.replace('-', '');
		sRemoteApiName = sRemoteApiName.replace('.', '');

		// Return API Name
		return sRemoteApiName.abbreviate(38);
	}

	public Integer getExcelDataSize()
	{
		return this.lAllExcelData.size();
	}

	public Integer getFullComponentSize()
	{
		return this.lFullComponentList.size();
	}

	public Integer getMetadataErrorSize()
	{
		return this.lAllMetadataErrors.size();
	}

	public Integer getValidComponentSize()
	{
		return this.lFullComponentList.size() - this.iComponentErrorCount;
	}
}