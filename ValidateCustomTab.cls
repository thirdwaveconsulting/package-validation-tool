/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Implementation of MetadataComponentValidator for fetching & validating deployment package components
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-11-16		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class ValidateCustomTab implements PackageValidationGenerator.MetadataComponentValidator
{
	/////////////////////////////////////////////////////////////////////
	// * VALIDATE METADATA COMPONENTS AGAINST COMPONENT MAP PROVIDED * //
	/////////////////////////////////////////////////////////////////////

	public static void ValidateComponents(List<PackageValidationWrapper.ValidationLineWrapper> lComponents, Map<String, Map<String, String>> mapValidComponents)
	{
		// Run Through Components
		for(PackageValidationWrapper.ValidationLineWrapper oOneWrapper : lComponents)
		{
			// Component Found?
			if(oOneWrapper.sEntityApiName != null && mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()) != null)
			{
				// Set NOT FOUND Flag --> Valid!
				oOneWrapper.setNotFound(false);

				// Assign Prefix & Suffix
				oOneWrapper.setComponentPrefix(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_PREFIX));
				oOneWrapper.setComponentSuffix(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_SUFFIX));

				// Assign to Final Component with fixed Casing
				oOneWrapper.setFinalComponentName(mapValidComponents.get(oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_NAMES));
			}

			// Set Processed Flag
			oOneWrapper.setIsProcessed(true);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	// * RETRIEVE & BUILD MAP OF VALID COMPONENTS FROM ENDPOINT PROVIDED ON WRAPPER * //
	////////////////////////////////////////////////////////////////////////////////////

	public static Map<String, Map<String, String>> RetrieveValidComponents(PackageValidationWrapper oWrapper, List<PackageValidationWrapper.ValidationLineWrapper> lComponents)
	{
		// Initialize Map for Storing valid Components
		Map<String, Map<String, String>> mapValidComponents = new Map<String, Map<String, String>>();

		// Run Through Components
		for(String sComponentName : oWrapper.mapTabList.values())
		{
			// Initialize Component Sub-Map
			mapValidComponents.put(sComponentName.toLowerCase(), new Map<String, String>());

			// Add Component Name, Prefix & Suffix to Map
			mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_NAMES, sComponentName);
			mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_PREFIX, '');
			mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_SUFFIX, '');
		}

		// Return Valid Components Map
		return mapValidComponents;
	}

	/////////////////////////////////////////////////
	// * GENERATE XML NAME FOR A GIVEN COMPONENT * //
	/////////////////////////////////////////////////

	public static String GenerateXmlName(String sMetadataType, String sEntityApiName, String sEntityLabel, String sObjectApiName)
	{
		// Initialize XML Name
		String sXmlName = sEntityApiName;

		// Return Final Value
		return sXmlName;
	}
}