/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Utility Class for generating deployment packages (XML)
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-10-31		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class PackageValidationGenerator
{
	// **************************** //
	// ** VALIDATION MAP INDICES ** //
	// **************************** //

	public static final String VALIDATION_CLASS_PREFIX = 'Validate';
	public static final String VALIDATION_MAP_NAMES = 'valid_component_names';
	public static final String VALIDATION_MAP_PREFIX = 'valid_component_prefix';
	public static final String VALIDATION_MAP_SUFFIX = 'valid_component_suffix';

	////////////////////////////////////////////
	// * GENERATE XML NAME FOR A GIVEN LINE * //
	////////////////////////////////////////////

	public static String GenerateXmlName(String sMetadataType, String sEntityApiName, String sEntityLabel, String sObjectApiName)
	{
		// Initialize XML Name
		String sXmlName = sEntityApiName;

		// Interface Implementation & Type to store Class Names
		PackageValidationGenerator.MetadataComponentValidator oImplementation;
		Type oMetadataType = Type.forName(VALIDATION_CLASS_PREFIX + sMetadataType);

		try
		{
			// Get Validation Apex Class
			oImplementation = (PackageValidationGenerator.MetadataComponentValidator)oMetadataType.newInstance();
		}
		catch(NullPointerException ex)
		{
			// Do nothing
		}

		// Get Components XML Name
		sXmlName = (oImplementation == null) ? sXmlName : oImplementation.GenerateXmlName(sMetadataType, sEntityApiName, sEntityLabel, sObjectApiName);

		// Return Final Value
		return sXmlName.trim();
	}

	////////////////////////////////////////////////////////////////////////////
	// * GENERATE COMPLETE VALID COMPONENTs MAP WITH METADATA LIST PROVIDED * //
	////////////////////////////////////////////////////////////////////////////

	public static Map<String, Map<String, Map<String, String>>> GenerateFullValidComponentsMap(Map<String, List<PackageValidationWrapper.ValidationLineWrapper>> mapComponents, PackageValidationWrapper oWrapper, List<String> lMetadataTypes)
	{
		// Initialize Valid Components Map
		Map<String, Map<String, Map<String, String>>> mapValidComponents = new Map<String, Map<String, Map<String, String>>>();

		// Interface Implementation & Type to store Class Names
		PackageValidationGenerator.MetadataComponentValidator oImplementation;
		Type oMetadataType;

		// Run Through Metadata Types
		for(String sMetadataType : lMetadataTypes)
		{
			try
			{
				// Set Type for Metadata
				oMetadataType = Type.forName(VALIDATION_CLASS_PREFIX + sMetadataType);

				// Get Validation Apex Class
				oImplementation = (PackageValidationGenerator.MetadataComponentValidator)oMetadataType.newInstance();

				// Assign valid components to Map
				mapValidComponents.put(sMetadataType, new Map<String, Map<String, String>>(oImplementation.RetrieveValidComponents(oWrapper, mapComponents.get(sMetadataType))));
			}
			catch(NullPointerException ex)
			{
				// Do nothing
			}
		}

		// Return Final Map
		return mapValidComponents;
	}

	/////////////////////////////////////////
	// * VALIDATE ALL PACKAGE COMPONENTS * //
	/////////////////////////////////////////

	public static void ValidatePackageComponents(PackageValidationWrapper oWrapper, Map<String, List<PackageValidationWrapper.ValidationLineWrapper>> mapComponents, Map<String, Map<String, Map<String, String>>> mapFullValidationData)
	{
		// Initialize Component List (per type)
		List<PackageValidationWrapper.ValidationLineWrapper> lComponents = new List<PackageValidationWrapper.ValidationLineWrapper>();

		// Interface Implementation & Type to store Class Names
		PackageValidationGenerator.MetadataComponentValidator oImplementation;
		Type oMetadataType;

		// Run Through Component Map
		for(String sMetadataType : mapFullValidationData.keySet())
		{
			// Get Class Name for this Metadata Type
 			oMetadataType = Type.forName(VALIDATION_CLASS_PREFIX + sMetadataType);

			// Get Component List for this Metadata Type
			lComponents = mapComponents.get(sMetadataType);

			try
			{
				// Get Validation Apex Class
				oImplementation = (PackageValidationGenerator.MetadataComponentValidator)oMetadataType.newInstance();

				// Validate components
				oImplementation.ValidateComponents(lComponents, mapFullValidationData.get(sMetadataType));
			}
			catch(NullPointerException ex)
			{
				// Do nothing, continue to next Metadata Type
			}
		}
	}

	///////////////////////////////////////////////
	// * BUILD XML PACKAGE FROM VALIDATED DATA * //
	///////////////////////////////////////////////

	public static DOM.Document BuildFinalXMLPackage(Map<String, List<PackageValidationWrapper.ValidationLineWrapper>> mapComponents, Decimal dApiVersion)
	{
		// Initialize Package Data Map
		Map<String, List<String>> mapPackageData = new Map<String, List<String>>();

		// Individual Component List
		List<PackageValidationWrapper.ValidationLineWrapper> lComponentList;
		List<String> lSingleList;

		// Run Through Metadata Types
		for(String sOneType : mapComponents.keySet())
		{
			// Get Component List & Initialize New List 
			lComponentList = (mapComponents.get(sOneType) != null) ? mapComponents.get(sOneType) : new List<PackageValidationWrapper.ValidationLineWrapper>();
			lSingleList = new List<String>();

			// Run Through Wrappers
			for(PackageValidationWrapper.ValidationLineWrapper oOneWrapper : lComponentList)
			{
				// Valid Component?
				if(oOneWrapper.bNotFound == false && oOneWrapper.sFinalComponentName != null && oOneWrapper.sFinalComponentName.trim() != '')
				{
					// Add Component to List
					lSingleList.add(oOneWrapper.sComponentPrefix + oOneWrapper.sFinalComponentName + oOneWrapper.sComponentSuffix);
				}
			}

			// Components were added?
			if(lSingleList.size() > 0)
			{
				// Add List to Package Map
				mapPackageData.put(sOneType, lSingleList);
			}
		}

		// Return XML Document
		return GenerateDeploymentPackage(mapPackageData, dApiVersion);
	}

	//////////////////////////////////////////////////////////////////
	// * GENERATE DEPLOYMENT PACKAGE STRING WITH GIVEN COMPONENTS * //
	//////////////////////////////////////////////////////////////////

	public static DOM.Document GenerateDeploymentPackage(Map<String, List<String>> mapPackage, Decimal dApiVersion)
	{
		// Initialize Package Document & Set Entity Node
		DOM.Document oPackageDocument = new DOM.Document();
		DOM.XmlNode oEntityNode;

		// Create Root Element
		oPackageDocument.createRootElement('Package', PackageValidationUtilities.TARGET_NAMESPACE_METADATA, '');

		// List of Metadata & Counter
		List<String> lMetadata;
		Integer iCount;

		// Run Through Metadata Types
		for(String sOneType : mapPackage.keySet())
		{
			// Get List of Metadata & Sort Ascending (CASE INSENSITIVE)
			lMetadata = PackageValidationUtilities.SortListCaseInsensitive(mapPackage.get(sOneType));

			// Create Section for Metadata Type
			oEntityNode = oPackageDocument.getRootElement().addChildElement('types', null, null);

			// Run Through Metadata Elements
			for(iCount = 0; iCount < lMetadata.size(); iCount++)
			{
				// Add Member Element 
				oEntityNode.addChildElement('members', null, null).addTextNode(lMetadata[iCount]);
			}

			// Add Name Element (For Metadata Type)
			oEntityNode.addChildElement('name', null, null).addTextNode(sOneType);
		}

		// Create Version Tag at End of Package XML
		oPackageDocument.getRootElement().addChildElement('version', null, null).addTextNode(dApiVersion.setScale(0).toPlainString() + '.0');

		// Return Final Document
		return oPackageDocument;
	}

	/////////////////////////////////////////////////////////
	// * GENERATE SINGLE REMOTE SITE SETTING ENTRY (XML) * //
	/////////////////////////////////////////////////////////

	public static DOM.Document GenerateSingleRemoteSiteSetting(String sSiteUrl, Boolean bDisableSecurity, Boolean bIsActive)
	{
		// Initialize Remote Site Document & Root Element
		DOM.Document oSiteDocument = new DOM.Document();
		DOM.XmlNode oRoot;

		// Create Root Element
		oRoot = oSiteDocument.createRootElement('RemoteSiteSetting', PackageValidationUtilities.TARGET_NAMESPACE_METADATA, '');

		// Add Elements for Remote Site
		oRoot.addChildElement('disableProtocolSecurity', null, null).addTextNode((bDisableSecurity == true) ? 'true' : 'false');
		oRoot.addChildElement('isActive', null, null).addTextNode((bIsActive == true) ? 'true' : 'false');
		oRoot.addChildElement('url', null, null).addTextNode(sSiteUrl);

		// Return Final Document
		return oSiteDocument;
	}

	////////////////////////////////////////////////////
	// * CLEAN API NAME (REMOVE INVALID CHARACTERS) * //
	////////////////////////////////////////////////////

	public static String CleanApiName(String sOriginalValue)
	{
		// Cleaned String
		String sCleanString;

		// Remove Extra Spaces
		sCleanString = sOriginalValue.replaceAll('\\s+', '');

		// Return Clean String
		return sCleanString;
	}

	///////////////////////////////////////////////////////
	// * INTERFACE FOR INDIVIDUAL COMPONENT VALIDATION * //
	///////////////////////////////////////////////////////

	public interface MetadataComponentValidator
	{
		// Validate given components against a Map of valid components & Return any errors found (Component List)
		void ValidateComponents(List<PackageValidationWrapper.ValidationLineWrapper> lComponents, Map<String, Map<String, String>> mapValidComponents);

		// Method for getting Map of valid components for a specific Metadata Type
		Map<String, Map<String, String>> RetrieveValidComponents(PackageValidationWrapper oWrapper, List<PackageValidationWrapper.ValidationLineWrapper> lComponents);

		// Generate XML Name used in Packages for this Component based on its Metadata Type
		String GenerateXmlName(String sMetadataType, String sEntityApiName, String sEntityLabel, String sObjectApiName);
	}
}