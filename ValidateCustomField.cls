/*
	---------------------------------------------------------------------------------------------------------------------------
	Author:			Gui Manders - Third Wave Consulting Inc.
	Company:		TWC
	Description:	Implementation of MetadataComponentValidator for fetching & validating deployment package components
	Test Class:		N/A

	History
	<Date>			<Authors Name>		<Brief Description of Change>
	2016-11-16		Gui Manders			Class Creation
	---------------------------------------------------------------------------------------------------------------------------
*/

public class ValidateCustomField implements PackageValidationGenerator.MetadataComponentValidator
{
	/////////////////////////////////////////////////////////////////////
	// * VALIDATE METADATA COMPONENTS AGAINST COMPONENT MAP PROVIDED * //
	/////////////////////////////////////////////////////////////////////

	public static void ValidateComponents(List<PackageValidationWrapper.ValidationLineWrapper> lComponents, Map<String, Map<String, String>> mapValidComponents)
	{
		// Run Through Components
		for(PackageValidationWrapper.ValidationLineWrapper oOneWrapper : lComponents)
		{
			// Component Found?
			if(mapValidComponents.get(oOneWrapper.sObjectApiName.toLowerCase() + '.' + oOneWrapper.sEntityApiName.toLowerCase()) != null)
			{
				// Set NOT FOUND Flag --> Valid!
				oOneWrapper.setNotFound(false);

				// Assign Prefix & Suffix
				oOneWrapper.setComponentPrefix(mapValidComponents.get(oOneWrapper.sObjectApiName.toLowerCase() + '.' + oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_PREFIX));
				oOneWrapper.setComponentSuffix(mapValidComponents.get(oOneWrapper.sObjectApiName.toLowerCase() + '.' + oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_SUFFIX));

				// Assign to Final Component with fixed Casing
				oOneWrapper.setFinalComponentName(mapValidComponents.get(oOneWrapper.sObjectApiName.toLowerCase() + '.' + oOneWrapper.sEntityApiName.toLowerCase()).get(PackageValidationGenerator.VALIDATION_MAP_NAMES).subStringAfter('.'));
			}

			// Set Processed Flag
			oOneWrapper.setIsProcessed(true);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	// * RETRIEVE & BUILD MAP OF VALID COMPONENTS FROM ENDPOINT PROVIDED ON WRAPPER * //
	////////////////////////////////////////////////////////////////////////////////////

	public static Map<String, Map<String, String>> RetrieveValidComponents(PackageValidationWrapper oWrapper, List<PackageValidationWrapper.ValidationLineWrapper> lComponents)
	{
		// Initialize Map for Storing valid Components
		Map<String, Map<String, String>> mapValidComponents = new Map<String, Map<String, String>>();

		// Metadata API Response, Describe Response & Result Node
		HttpResponse oResponse;
		DOM.XmlNode oDescribeResponse;
		DOM.XmlNode oResultNode;

		// Initialize Set of Unique Objects & Set of Field Names (per Object)
		Set<String> setObjects = new Set<String>();
		Set<String> setFields;

		// Object API Name String & Component Name
		String sObjectApiName;
		String sComponentName;

		// Run Through Components
		for(PackageValidationWrapper.ValidationLineWrapper oOneWrapper : lComponents)
		{
			// Object name NOT found in Set AND Is Valid?
			if(oOneWrapper.sObjectApiName != null && !setObjects.contains(oOneWrapper.sObjectApiName.toLowerCase()) && oWrapper.mapObjectList.get(oOneWrapper.sObjectApiName.toLowerCase()) != null)
			{
				// Add to Set
				setObjects.add(oOneWrapper.sObjectApiName.toLowerCase());
			}
		}

		// Send Describe Single SObject Request & Get Result Node
		oResponse = PackageValidationUtilities.SendDescribeSObjectsRequest(oWrapper.sAccessToken, oWrapper.sAccessEndpoint, setObjects, oWrapper.dApiVersion);
		oDescribeResponse = oResponse.getBodyDocument().getRootElement().getChildElement('Body', PackageValidationUtilities.SOAP_NAMESPACE).getChildElement('describeSObjectsResponse', PackageValidationUtilities.TARGET_NAMESPACE_ENTERPRISE);

		for(DOM.XmlNode oOneResponse : oDescribeResponse.getChildElements())
		{
			// Object Data Found?
			if(oOneResponse.getName() == 'result')
			{
				// Initialize Field List
				setFields = new Set<String>();

				// Run Through Individual Object Data
				for(DOM.XmlNode oOneObjectData : oOneResponse.getChildElements())
				{
					// Object Field Definition?
					if(oOneObjectData.getName() == 'fields')
					{
						// Run Through Field Settings
						for(DOM.XmlNode oOneField : oOneObjectData.getChildElements())
						{
							// Field API Name?
							if(oOneField.getName() == 'name')
							{
								// Add Field API Name to Set
								setFields.add(oOneField.getText());
							}
						}
					}
					else if(oOneObjectData.getName() == 'name')
					{
						// Get Correct Object API Name
						sObjectApiName = oOneObjectData.getText();
					}
				}

				// Run Through All Fields Found
				for(String sOneField : setFields)
				{
					// Get Component Name
					sComponentName = sObjectApiName + '.' + sOneField;

					// Initialize Component Sub-Map
					mapValidComponents.put(sComponentName.toLowerCase(), new Map<String, String>());

					// Add Component Name, Prefix & Suffix to Map
					mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_NAMES, sComponentName);
					mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_PREFIX, sObjectApiName + '.');
					mapValidComponents.get(sComponentName.toLowerCase()).put(PackageValidationGenerator.VALIDATION_MAP_SUFFIX, '');
				}
			}
		}

		// Return Valid Components Map
		return mapValidComponents;
	}

	/////////////////////////////////////////////////
	// * GENERATE XML NAME FOR A GIVEN COMPONENT * //
	/////////////////////////////////////////////////

	public static String GenerateXmlName(String sMetadataType, String sEntityApiName, String sEntityLabel, String sObjectApiName)
	{
		// Initialize XML Name
		String sXmlName = sEntityApiName;

		// Return Final Value
		return sXmlName;
	}
}